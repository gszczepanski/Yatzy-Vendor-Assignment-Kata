#!/bin/bash
vendors=(org.yatzy.cli.CliAdapterYatzy1 org.yatzy.cli.CliAdapterYatzy2 org.yatzy.cli.CliAdapterYatzy3)

for vendor in ${vendors[*]}
do 	
	echo "------[YATZY VENDOR: $vendor]------"
	mvn exec:java -Dexec.mainClass="$vendor" < src/it/texttest/Chance/stdin.yatzy
done

