package org.yatzy;

import java.util.List;

import static java.util.Arrays.asList;

public enum YatzyCategory {

    CHANCE,
    YATZY,
    ONES,
    TWOS,
    THREES,
    FOURS,
    FIVES,
    SIXES,
    PAIR,
    THREEOFAKIND,
    FOUROFAKIND,
    SMALLSTRAIGHT,
    LARGESTRAIGHT,
    TWOPAIRS,
    FULLHOUSE;

    public static YatzyCategory from(String category) {
        return valueOf(category.toUpperCase());
    }

    public static List<YatzyCategory> validCategories() {
        return asList(values());
    }
}
