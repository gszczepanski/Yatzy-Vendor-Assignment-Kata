package org.yatzy;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class RollInput {

    private final String line;
    private final Dice dice;
    private final YatzyCategory category;

    /**
     * Create RollInput object from parsed line.
     * <br>
     * Line should have numbers separated by comma, and then after space second param, which mean YatzyCategory.
     * <br>
     * Examples:
     * <br>
     * 1,1,3,3,6 CHANCE
     * <br>
     * 1,2,3,4,1 SIXES
     * <br>
     * 5,5,3,5,1 PAIR
     */
    public static RollInput fromLine(String line) {
        checkState(!isEmpty(line), "Given line should be not empty nor null");

        String[] fields = line.split(SPACE);
        Dice dice = Dice.fromParsedLine(fields[0]);

        YatzyCategory category = YatzyCategory.from(fields[1]);
        return new RollInput(line, dice, category);
    }

}


