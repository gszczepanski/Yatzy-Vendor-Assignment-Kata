package org.yatzy.cli;

import lombok.RequiredArgsConstructor;
import org.yatzy.RollInput;
import org.yatzy.YatzyCalculator;

@RequiredArgsConstructor
class YatzyGameRunner {

    private final YatzyCalculator yatzy;

    void runGame() {
        InputReader rollReader = new InputReader();
        while (rollReader.hasNext()) {
            String inputLine = rollReader.nextLine();
            RollInput roll = yatzy.parseDiceAndCategory(inputLine);
            int score = yatzy.score(roll.getDice(), roll.getCategory());
            System.out.println("[" + roll.getLine().trim() + "] \"" + roll.getCategory() + "\": " + score);
        }
    }

}
