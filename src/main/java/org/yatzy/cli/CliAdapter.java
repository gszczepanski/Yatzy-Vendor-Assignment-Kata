package org.yatzy.cli;

import org.yatzy.YatzyCalculator;

public class CliAdapter {

    public static void main(String[] args, YatzyCalculator yatzy) {
        if (printHelpRequired(args)) {
            HelpCommand.printHelpForYatzy(yatzy);
        }

        YatzyGameRunner yatzyGameRunner = new YatzyGameRunner(yatzy);
        yatzyGameRunner.runGame();
    }

    private static boolean printHelpRequired(String[] args) {
        return args.length > 0 && "--help".equals(args[0]);
    }
}
