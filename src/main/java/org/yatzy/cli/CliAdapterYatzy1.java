package org.yatzy.cli;

import org.yatzy.vendor1.FirstYatzy;
import org.yatzy.vendor1.ScoreFactory;

public class CliAdapterYatzy1 {

    public static void main(String[] args){
        CliAdapter.main(args,
                new FirstYatzy(
                        new ScoreFactory()
                )
        );
    }
}
