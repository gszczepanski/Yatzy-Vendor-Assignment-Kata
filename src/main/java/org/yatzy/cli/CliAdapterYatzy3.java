package org.yatzy.cli;

import org.yatzy.vendor3.ThirdYatzy;

public class CliAdapterYatzy3 {
    public static void main(String[] args){
        CliAdapter.main(args, new ThirdYatzy());
    }
}