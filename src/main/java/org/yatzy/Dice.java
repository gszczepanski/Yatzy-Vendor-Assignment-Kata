package org.yatzy;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Arrays.*;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Dice {

    public static final List<Integer> DICE_VALUES = asList(6, 5, 4, 3, 2, 1);

    private final List<Integer> values;

    public static Dice fromParsedLine(String line) {
        String[] numbers =  splitNumbersFromLine(line);
        validateParsedNumbersArray(numbers);

        List<Integer> values = stream(numbers)
                .map(Integer::parseInt)
                .peek(Dice::validateParsedDiceNumber)
                .collect(toList());

        return new Dice(unmodifiableList(values));
    }

    private static String[] splitNumbersFromLine(String line) {
        checkState(!isEmpty(line), "Given line should be not empty nor null");
        return line.split(",");
    }

    private static void validateParsedNumbersArray(String[] numbers) {
        checkState(numbers != null, "Given numbers array must not be null");
        checkState(numbers.length != 0, "Given numbers array must not be empty");
    }

    private static void validateParsedDiceNumber(Integer number) {
        if (!DICE_VALUES.contains(number)) {
            throw new IllegalArgumentException("Dice value must be in range from 1 to 6!");
        }
    }

    // TEMPORARY TO AVOID COMPILATION ERROR ON YATZY 2
    public int get(int index) {
        return values.get(index);
    }

}
