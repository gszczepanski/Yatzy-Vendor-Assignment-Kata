package org.yatzy.vendor1;

import org.yatzy.Dice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static org.yatzy.Dice.*;

abstract class Score {

    abstract int calculate(Dice dice);

    protected int sum(Dice dice) {
        return dice.getValues()
                    .stream()
                        .mapToInt(Integer::intValue)
                        .sum();
    }

    protected Map<Integer, Integer> frequencies(Dice dice) {
        HashMap<Integer, Integer> frequencies = new HashMap<>();
        for (int i : DICE_VALUES) {
            frequencies.put(i, 0);
        }
        for (int die : dice.getValues()) {
            frequencies.put(die, frequencies.get(die) + 1);
        }

        return frequencies;
    }

    protected int numberFrequency(int number, Dice dice) {
        return frequencies(dice).get(number) * number;
    }

    protected int nofakind(int n, Dice dice) {
        Map<Integer, Integer> frequencies = frequencies(dice);
        for (int i : Arrays.asList(5, 4, 3, 2, 1, 6)) {
            if (frequencies.get(i) >= n) {
                return i * n;
            }
        }
        return 0;
    }

    protected boolean isStraight(Dice dice) {
        return frequencies(dice)
                    .values()
                        .stream()
                            .filter(f -> f == 1)
                            .collect(toList()).size() == 5;
    }
}
