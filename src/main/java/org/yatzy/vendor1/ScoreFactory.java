package org.yatzy.vendor1;

import lombok.RequiredArgsConstructor;
import org.yatzy.Dice;
import org.yatzy.YatzyCategory;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static org.yatzy.Dice.DICE_VALUES;

public class ScoreFactory {

    Score scoreFor(YatzyCategory category) {
        Score score;

        switch (category) {
            case CHANCE:
                score = new ChanceScore();
                break;
            case YATZY:
                score = new YatzyScore();
                break;
            case ONES:
                score = new NumberScore(1);
                break;
            case TWOS:
                score = new NumberScore(2);
                break;
            case THREES:
                score = new NumberScore(3);
                break;
            case FOURS:
                score = new NumberScore(4);
                break;
            case FIVES:
                score = new NumberScore(5);
                break;
            case SIXES:
                score = new NumberScore(6);
                break;
            case PAIR:
                score = new PairScore();
                break;
            case THREEOFAKIND:
                score = new ThreeOfAKindScore();
                break;
            case FOUROFAKIND:
                score = new FourOfAKindScore();
                break;
            case SMALLSTRAIGHT:
                score = new SmallStraightScore();
                break;
            case LARGESTRAIGHT:
                score = new LargeStraightScore();
                break;
            case TWOPAIRS:
                score = new TwoPairsScore();
                break;
            case FULLHOUSE:
                score = new FullHouseScore();
                break;
            default:
                throw new UnknownCategoryException(category.toString());
        }

        return score;
    }

    private static class ChanceScore extends Score {
        @Override
        public int calculate(Dice dice) {
            return sum(dice);
        }
    }

    private static class YatzyScore extends Score {
        @Override
        public int calculate(Dice dice) {
            if (frequencies(dice).containsValue(5)) {
                return 50;
            }
            return 0;
        }
    }

    @RequiredArgsConstructor
    private static class NumberScore extends Score {

        private final int number;

        @Override
        public int calculate(Dice dice) {
            return numberFrequency(number, dice);
        }
    }

    private static class PairScore extends Score {
        @Override
        public int calculate(Dice dice) {
            return nofakind(2, dice);
        }
    }

    private static class ThreeOfAKindScore extends Score {
        @Override
        public int calculate(Dice dice) {
            return nofakind(3, dice);
        }
    }

    private static class FourOfAKindScore extends Score {
        @Override
        public int calculate(Dice dice) {
            return nofakind(4, dice);
        }
    }

    private static class SmallStraightScore extends Score {
        @Override
        public int calculate(Dice dice) {
            if (isStraight(dice) && frequencies(dice).get(6) == 0) {
                return sum(dice);
            }
            return 0;
        }
    }

    private static class LargeStraightScore extends Score {
        @Override
        public int calculate(Dice dice) {
            if (isStraight(dice) && frequencies(dice).get(1) == 0) {
                return sum(dice);
            }
            return 0;
        }
    }

    private static class TwoPairsScore extends Score {
        @Override
        public int calculate(Dice dice) {
            Map<Integer, Integer> frequencies = frequencies(dice);
            int score = 0;
            if (frequencies(dice).values().stream().filter(f -> f == 2).collect(toList()).size() == 2) {
                for (int i : DICE_VALUES) {
                    if (frequencies.get(i) >= 2) {
                        score += i * 2;
                    }
                }
            }
            return score;
        }
    }

    private static class FullHouseScore extends Score {
        @Override
        public int calculate(Dice dice) {
            Map<Integer, Integer> frequencies = frequencies(dice);
            if (frequencies.values().contains(2) && frequencies.values().contains(3)) {
                return sum(dice);
            }
            return 0;
        }
    }

}
