package org.yatzy.vendor1;

import lombok.RequiredArgsConstructor;
import org.yatzy.Dice;
import org.yatzy.RollInput;
import org.yatzy.YatzyCalculator;
import org.yatzy.YatzyCategory;

import java.util.List;

@RequiredArgsConstructor
public class FirstYatzy implements YatzyCalculator {

    private final ScoreFactory scoreFactory;

    @Override
    public List<YatzyCategory> validCategories() {
        return YatzyCategory.validCategories();
    }

    @Override
    public RollInput parseDiceAndCategory(String line) {
        return RollInput.fromLine(line);
    }

    @Override
    public int score(Dice dice, YatzyCategory category) {
        Score score = scoreFactory.scoreFor(category);
        return score.calculate(dice);
    }

}

