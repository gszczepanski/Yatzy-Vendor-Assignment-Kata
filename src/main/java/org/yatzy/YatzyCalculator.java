package org.yatzy;

import java.util.List;

public interface YatzyCalculator {

    List<YatzyCategory> validCategories();

    RollInput parseDiceAndCategory(String inputLine);

    int score(Dice dice, YatzyCategory category);

}

