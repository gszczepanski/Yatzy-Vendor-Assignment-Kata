package org.yatzy;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DiceTest {

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenLineGivenForParseIsBlank() {
        Dice.fromParsedLine("");
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenLineGivenForParseIsNull() {
        Dice.fromParsedLine(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenNumberGivenToParseIsNotDiceNumber() {
        Dice.fromParsedLine("1,2,3,4,5,99");
    }

    @Test
    public void shouldCreateDiceWithValuesFromStringInCorrectOrder() {
        Dice dice = Dice.fromParsedLine("1,2,3,4,5,6");

        assertThat(dice.getValues()).containsSequence(1, 2, 3, 4, 5, 6);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldDiceValuesListBeImmutable() {
        Dice dice = Dice.fromParsedLine("1,2,3,4,5,6");

        dice.getValues().add(102);
    }

}