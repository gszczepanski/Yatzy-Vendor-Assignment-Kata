package org.yatzy.vendor1;

import org.junit.Before;
import org.junit.Test;
import org.yatzy.RollInput;
import org.yatzy.YatzyCategory;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.yatzy.RollInput.fromLine;

public class FirstYatzyTest {

    FirstYatzy yatzy;

    @Before
    public void setup() {
        yatzy = new FirstYatzy(new ScoreFactory());
    }

    @Test
    public void shouldReturnAllValidCategories() {
        List<YatzyCategory> allCategories = Arrays.asList(YatzyCategory.values());

        assertThat(yatzy.validCategories())
                .containsAll(allCategories);
    }

    @Test
    public void shouldReturnValidRollInputOnParseDiceAndCategory() {
        String line = "1,1,3,3,6 CHANCE";

        RollInput rollInput = yatzy.parseDiceAndCategory(line);

        assertThat(rollInput)
                .isEqualTo(validRollInput(line));
    }

    private RollInput validRollInput(String line) {
        return fromLine(line);
    }


}