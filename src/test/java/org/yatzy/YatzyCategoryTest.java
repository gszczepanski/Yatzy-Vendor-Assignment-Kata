package org.yatzy;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.yatzy.YatzyCategory.CHANCE;

public class YatzyCategoryTest {

    @Test
    public void shouldCreateCategoryFromLowercaseString() {
        Assertions.assertThat(YatzyCategory.from("chance"))
                .isEqualTo(CHANCE);
    }

    @Test
    public void shouldCreateCategoryFromUppercaseString() {
        assertThat(YatzyCategory.from("CHANCE"))
                .isEqualTo(CHANCE);
    }

    @Test
    public void shouldReturnAllCategories() {
        List<YatzyCategory> yatzyCategories = asList(YatzyCategory.values());

        assertThat(YatzyCategory.validCategories())
                        .containsAll(yatzyCategories);
    }
}