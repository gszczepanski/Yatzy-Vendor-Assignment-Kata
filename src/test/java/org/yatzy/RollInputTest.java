package org.yatzy;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.yatzy.YatzyCategory.CHANCE;

public class RollInputTest {

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenLinePassedToParseIsNull() {
        RollInput.fromLine(null);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenLinePassedToParseIsBlank() {
        String BLANK = "";
        RollInput.fromLine(BLANK);
    }

    @Test
    public void shouldParseDiceNumbersInCorrectOrder() {
        String line = "1,1,3,3,6 chance";

        RollInput rollInput = RollInput.fromLine(line);

        assertThat(rollInput.getDice().getValues())
                .containsSequence(1, 1, 3, 3, 6);
    }

    @Test
    public void shouldParseToRightCategory() {
        String line = "1,1,3,3,6 CHANCE";

        RollInput rollInput = RollInput.fromLine(line);

        assertThat(rollInput.getCategory())
                .isEqualTo(CHANCE);
    }

    @Test
    public void shouldParseToRightCategoryEvenIfCategoryLettersAreLowercase() {
        String line = "1,1,3,3,6 chance";

        RollInput rollInput = RollInput.fromLine(line);

        assertThat(rollInput.getCategory())
                .isEqualTo(CHANCE);
    }

}