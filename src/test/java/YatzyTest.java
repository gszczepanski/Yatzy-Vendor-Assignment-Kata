import org.junit.Before;
import org.junit.Test;
import org.yatzy.Dice;
import org.yatzy.vendor1.FirstYatzy;
import org.yatzy.vendor1.ScoreFactory;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.yatzy.YatzyCategory.CHANCE;

public class YatzyTest {

    private FirstYatzy calculator;

    @Before
    public void setUp() {
        calculator = new FirstYatzy(
                new ScoreFactory()
        );
    }

    private void assertScore(int expected, String category, Dice dice) {

        assertEquals(expected, calculator.score(dice, CHANCE));
    }

    @Test
    public void chanceSumsTheDice() {
        assertScore(5, "CHANCE", Dice.fromParsedLine("1,1,1,1,1"));
    }




}
